# encoding: utf-8

stack_outputs = json(content: inspec.profile.file('stack-outputs.json'))

vpc_id = ''
instance_id = ''
pub_subnet_a_id = ''
instance_sg_id = ''

stack_outputs.each do |output|
  case output['OutputKey']
  when 'VPCOutput'
    vpc_id = output['OutputValue']
  when 'PublicSubnetAOutput'
    pub_subnet_a_id = output['OutputValue']
  when 'InstanceSGOutput'
    instance_sg_id = output['OutputValue']
  when 'InstanceIdOutput'
    instance_id = output['OutputValue']
  else
    next
  end
end

title 'Networking'

control 'vpc' do
  impact 1.0
  title 'Ensure VPC'
  desc 'VPC should exist, is available and has the correct cidr block'
  describe aws_vpc(vpc_id) do
    it { should exist }
    its('state') { should eq 'available' }
    its('cidr_block') { should cmp '10.101.0.0/16' }
  end
end

control 'subnet-pub-a' do
  impact 0.9
  title 'Ensure public subnet A'
  desc 'Subnet should exist, have the correct cidr block,
        be in the correct vpc, and be in the correct AZ'
  describe aws_subnet(pub_subnet_a_id) do
    it { should exist }
    its('cidr_block') { should cmp '10.101.21.0/24' }
    its('vpc_id') { should eq vpc_id }
    its('availability_zone') { should eq 'eu-west-1a' }
  end
end

title 'Instance'

control 'instance' do
  impact 0.9
  title 'Ensure Instance'
  desc 'Ensure that the instance exists,
        is running, is in the right subnet,
        has the correct instance type, the correct AMI (image id)
        and has the correct security group'
  describe aws_ec2_instance(instance_id) do
    it { should exist }
    it { should be_running }
    its('subnet_id') { should eq pub_subnet_a_id }
    its('instance_type') { should eq 't2.micro' }
    its('image_id') { should eq 'ami-3bfab942' }
    its('security_group_ids') { should include(instance_sg_id) }
  end
end

control 'sg' do
  impact 1.0
  title 'Ensure Security Group'
  desc 'SG should be present, in the right VPC
        and not open to world on port 22'
  describe aws_security_group(instance_sg_id) do
    it { should exist }
    its('vpc_id') { should eq vpc_id }
    it { should_not allow_in(port: 22, ipv4_range: '0.0.0.0/0') }
  end
end